n = int(input("Dame un numero entero no negativo: "))
if n > 0:
    for i in range(2,n+1):
        creciente = 2
        esPrimo = True
        while esPrimo and creciente < i:
            if i % creciente == 0:
                esPrimo = False
            else:
                creciente += 1
        if esPrimo:
            print(i,"es primmo")
else:
    print("El numero ingresado es negativo. Intentelo nuevamente.")